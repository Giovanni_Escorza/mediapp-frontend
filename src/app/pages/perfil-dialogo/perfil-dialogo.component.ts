import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import * as decode from 'jwt-decode';
import { TOKEN_NAME } from 'src/app/_shared/var.constant';

@Component({
  selector: 'app-perfil-dialogo',
  templateUrl: './perfil-dialogo.component.html',
  styleUrls: ['./perfil-dialogo.component.css']
})
export class PerfilDialogoComponent implements OnInit {
  usuario:string = '';
  roles:string[] = [];

  constructor(private dialogRef: MatDialogRef<PerfilDialogoComponent>) { }

  ngOnInit() {
    let token = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
    const decodedToken = decode(token.access_token);
    this.usuario = decodedToken.user_name;
    this.roles = decodedToken.authorities;
  }

  cerrar() {
    this.dialogRef.close();
  }

}