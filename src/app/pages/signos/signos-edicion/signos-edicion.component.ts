
import { ActivatedRoute, Router, Params } from '@angular/router';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MatSnackBar, MatDialog } from '@angular/material';
import { map } from 'rxjs/operators';
import { PacienteDialogoComponent } from './paciente-dialogo/paciente-dialogo.component';
import { Signo } from 'src/app/_model/signo';
import { Paciente } from 'src/app/_model/paciente';
import { SignoService } from 'src/app/_service/signo.service';
import { PacienteService } from 'src/app/_service/paciente.service';

@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {

  id:number;
  form:FormGroup;
  myControlPaciente: FormControl = new FormControl();
  edicion:boolean = false;
  signo:Signo;
  pacientes:Array<Paciente> = [];

  filteredOptionsPaciente: Observable<any[]>;

  constructor(private builder: FormBuilder, private route:ActivatedRoute, private router: Router,
      private signoService:SignoService, private pacienteService:PacienteService,
      public snackbar:MatSnackBar,
      private dialog: MatDialog) {

        this.form =  builder.group({
          'id' : new FormControl(0),
          'fecha' : new FormControl(new Date()),
          'temperatura': new FormControl(0),
          'pulso': new FormControl(''),
          'ritmo': new FormControl(''),
          'paciente' : this.myControlPaciente
        }); 

  }

  ngOnInit() {
    this.listarPacientes();
    this.filteredOptionsPaciente = this.myControlPaciente.valueChanges.pipe(map(val => this.filterPaciente(val)));
    this.signo = new Signo();
    this.signo.idSignos = 0;
    this.route.params.subscribe((params:Params)=>{
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }

  private listarPacientes(){
    this.pacienteService.pacienteCambio.subscribe(data=>{
      this.pacientes = data;
    });
    this.pacienteService.listar().subscribe(data =>{
      this.pacientes = data;
    })
  }

  private filterPaciente(val:any){
    if(val != null && val.idPaciente > 0){
      return this.pacientes.filter(option => 
        option.nombres.toLocaleLowerCase().includes(val.nombres.toLocaleLowerCase()) || option.apellidos.toLocaleLowerCase().includes(val.apellidos.toLocaleLowerCase()) || option.dni.includes(val.dni));
    }else{
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }

  displayFnPaciente(val:Paciente){
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  seleccionarPaciente(e: any){
    this.signo.paciente = e.option.value;
  }

  private initForm(){
    if(this.edicion){
      this.signoService.listarSignoPorId(this.id).subscribe(data =>{
        this.myControlPaciente = new FormControl();
        this.myControlPaciente.setValue(data.paciente);
        this.filteredOptionsPaciente = this.myControlPaciente.valueChanges.pipe(map(val => this.filterPaciente(val)));

        this.form =  this.builder.group({
          'id' : new FormControl(data.idSignos),
          'fecha' : new FormControl(new Date(data.fecha)),
          'temperatura': new FormControl(data.temperatura),
          'pulso': new FormControl(data.pulso),
          'ritmo': new FormControl(data.ritmo),
          'paciente' : this.myControlPaciente
        });  
      })
    }
  }

  guardar(){
    let paciente = new Paciente();
    let _date = new Date(this.form.value['fecha']);
    let localISOTime = (_date).toISOString();
    this.signo.fecha = localISOTime;
    this.signo.idSignos = this.form.value['id'];
    this.signo.pulso = this.form.value['pulso'];
    this.signo.ritmo = this.form.value['ritmo'];
    this.signo.temperatura = this.form.value['temperatura'];
    paciente=this.form.value['paciente'];
    this.signo.paciente= paciente;
   console.log(paciente.idPaciente); 
   if(this.edicion){
      console.log(this.signo);
      this.signoService.modificar(this.signo).subscribe(data =>{
        this.signoService.listar().subscribe(signos =>{
          this.signoService.signoCambio.next(signos);
          this.signoService.mensajeCambio.next('Se modificó.');
        })
      })
    }else{
      this.signoService.registar(this.signo).subscribe(data =>{
        this.signoService.listar().subscribe(signos =>{
          this.signoService.signoCambio.next(signos);
          this.signoService.mensajeCambio.next('Se registró.');
        })
      })
    }
    this.router.navigate(['signo']);
  }

  openDialog(paciente?: Paciente){
    let pac = paciente != null ? paciente: new Paciente();
  this.dialog.open(PacienteDialogoComponent, {
    width: '400px',
    disableClose: true,
    data:pac
  });  
}

}
