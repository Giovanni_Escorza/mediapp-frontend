
import { Component, OnInit,Inject  } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Paciente } from 'src/app/_model/paciente';
import { PacienteService } from 'src/app/_service/paciente.service';
@Component({
  selector: 'app-paciente-dialogo',
  templateUrl: './paciente-dialogo.component.html',
  styleUrls: ['./paciente-dialogo.component.css']
})
export class PacienteDialogoComponent implements OnInit {

  paciente: Paciente;
  constructor(private dialogRef: MatDialogRef<PacienteDialogoComponent>, @Inject(MAT_DIALOG_DATA) public data: Paciente, private pacienteService: PacienteService) { }

  ngOnInit() {
    this.paciente = new Paciente();
    this.paciente.idPaciente = this.data.idPaciente;
    this.paciente.nombres = this.data.nombres;
    this.paciente.apellidos = this.data.apellidos;
    this.paciente.telefono = this.data.telefono;
    this.paciente.direccion = this.data.direccion;
    this.paciente.dni = this.data.dni;
    this.paciente.email = this.data.email;
  }

  cerrar() {
    this.dialogRef.close();
  }

  guardar() {
    console.log(this.paciente);
    this.pacienteService.registrar(this.paciente).subscribe(data => {
      this.pacienteService.listar().subscribe(pacientes => {
        this.pacienteService.pacienteCambio.next(pacientes);
        this.pacienteService.mensajeCambio.next('Se registró');
      })
    });
    this.dialogRef.close();

  }

}
