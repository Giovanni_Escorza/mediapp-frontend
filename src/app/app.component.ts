import { Menu } from './_model/menu';
import { LoginService } from './_service/login.service';
import { Component, OnInit } from '@angular/core';
import { MenuService } from './_service/menu.service';
import { PerfilDialogoComponent } from './pages/perfil-dialogo/perfil-dialogo.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'mediapp-frontend';

  menus: Menu[] = [];

  constructor(public loginService : LoginService, private menuService: MenuService,private dialog: MatDialog){
    
  }

  ngOnInit() {
    this.menuService.menuCambio.subscribe(data => {
      this.menus = data;
    });
  }
  openDialogPerfil(){
    this.dialog.open(PerfilDialogoComponent,{
      width: '400px',
      disableClose: true
    })
  }
}
