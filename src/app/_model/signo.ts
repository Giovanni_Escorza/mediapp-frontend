import { Paciente } from "./paciente";

export class Signo {
    idSignos: number;
    fecha: string;
    temperatura: number;
    pulso: string;
    ritmo: string;
    paciente: Paciente ;
}